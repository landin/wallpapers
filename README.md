# Simple, Clean, Elegant

My elegant and beautiful "portfolio" of wallpapers, i'm in a constant hunt of incredible wallpapers, use the ones that you love, or use all of them. I recommend you to clone the entire repo and search for the best of them
 --------------------------------------- 
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

## Animation
they are all the wallpapers that dont exactly fit on the other categories and are kinda cool :v

## Based
they are a combination of dark, cyberpunk, hackerman related stuff

## Nord
wallpapers with nord palette

## Tokyo Night
wallpapers with tokyo night palette

## Jpn
photos about japan, tokyo, osaka, etc

## Fantasy
medieval fantasy, etc

## Anime
i mean... anime wallpapers

## Paintings
the name is self explanatory

## Phone
phone wallpapers

## Real
real world photos
